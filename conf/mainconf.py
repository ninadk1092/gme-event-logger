from flask import Config
import os

class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://gme_user:pwd123abc@localhost/gme_db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JSONIFY_PRETTYPRINT_REGULAR = False

    API_VERSION = "/api/v0.1"

    SECRET_KEY = "justbags_69254Mnb0fe9353"

    LOCALHOST_URL = 'http://127.0.0.1:5000'
