#### Overview
Flask Application server for Logging Events in GME Arenas

#### Setup Instructions

* Create Virtual Environment
```
virtualenv gme_env
```

* Activate Virtual Environment
```
source gme_env/bin/active
```

* Install Dependencies
```
pip install -r requirements.txt
```

* Run Flask Server
```
python server.py
```
