from webapp import db, ma
import datetime
class Events_log(db.Model):
    __tablename__='events_log'
    id= db.Column(db.String(50),primary_key = True,nullable=False)
    event_id=db.Column(db.String(15))
    metadata=db.Column(db.String(15))
    timestamp=db.Column(db.DateTime,default=datetime.utcnow)
    

    def __init__(
            self, 
            id = None,
            event_id = None, 
            metadata = None, 
            timestamp = None, 
        ):
        self.id = id,
        self.event_id = event_id,
        self.metadata = metadata,
        self.timestamp = timestamp,
        

        def save_to_db(self):
         add_res = db.session.add(self)
         commit_res = db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

class Events_Schema(ma.ModelSchema):
    class Meta:
        model = Events_log