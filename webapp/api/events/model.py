from webapp import db, ma
import datetime
class Events(db.Model):
    __tablename__='events'
    id= db.Column(db.String(50),primary_key = True,nullable=False)
    events_name=db.Column(db.String(15))
    location=db.Column(db.String(15))
    start_date=db.Column(db.DateTime,default=datetime.utcnow)
    contact=db.Column(db.String(10))
    incharge=db.Column(db.String(15))
    status=db.Column(db.String(20))

    def __init__(
            self, 
            id = None,
            events_name = None, 
            location = None, 
            start_date = None, 
            contact=None,
            incharge=None,
            status=None
        ):
        self.id = id,
        self.events_name = events_name,
        self.location = location,
        self.start_date = start_date,
        self.contact = contact,
        self.incharge = incharge,
        self.status  =  status

        def save_to_db(self):
         add_res = db.session.add(self)
         commit_res = db.session.commit()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

class EventsSchema(ma.ModelSchema):
    class Meta:
        model = Events